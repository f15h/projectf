from PIL import Image
import pathlib


def concat_vertical(image1, image2):
    dst = Image.new('RGBA', (image1.width, image1.height + image2.height))
    dst.paste(image1, (0, 0))
    dst.paste(image2, (0, image2.height))
    return dst.copy()


def crop_lines(image):
    result = {}
    for i in range(0, 8):
        left = 0
        top = 0 + 32 * i
        right = 288
        bottom = 32 + 32 * i
        dst = image.crop((left, top, right, bottom))
        result[i] = dst
    return result


def contat_images(cropped_images):
    # todo: change to loop
    result_image1 = concat_vertical(cropped_images[2], cropped_images[3])
    result_image2 = concat_vertical(cropped_images[1], cropped_images[0])
    result_image3 = concat_vertical(cropped_images[6], cropped_images[7])
    result_image4 = concat_vertical(cropped_images[5], cropped_images[4])
    result_image5 = concat_vertical(result_image1, result_image2)
    result_image6 = concat_vertical(result_image3, result_image4)
    return concat_vertical(result_image5, result_image6)


def resize(original_image, value):
    width, height = original_image.size
    new_size = (width * value, height * value)
    return original_image.resize(new_size, Image.NEAREST)


def set_transpanent_color(original_image):
    im = original_image.convert('RGBA')
    datas = im.getdata()
    new_data = []
    color_to_replace = datas[0]

    for item in datas:
        if item == color_to_replace:
            new_data.append((255, 255, 255, 0))
        else:
            new_data.append(item)
    return im.putdata(new_data)


def get_output_filepath(original_filename):
    return pwd.joinpath('output').joinpath('charsets').joinpath(
        f'{original_filename.name.split(".")[0]}.png')  # get output folder path


def perform_convert_char(filename):
    # open image
    im = Image.open(filename)
    im = set_transpanent_color(im)
    cropped = crop_lines(im)
    result_image = contat_images(cropped)
    result_image = resize(result_image)
    result_image.save(get_output_filepath().resolve())


if __name__ == '__main__':

    # get files for
    pwd = pathlib.Path(__file__).parent.resolve()  # get path to root dir
    input_folder_path = pwd.joinpath('input').joinpath(
        'charsets')  # get input folder path
    input_files = input_folder_path.glob(
        '*.bmp')  # get all bpm files in folder

    for f in input_files:
        perform_convert_char(f)
