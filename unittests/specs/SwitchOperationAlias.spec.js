describe('SwitchOperationAlias', () => {
    it('should enabled switch switchID=1 via alias "sw[1]=1" switch', () => {
        expect($gameSwitches.value(1)).toEqual(false)
        new SwitchOperationAlias({
            index:1,
            value:1,
            operation: "=",
            type: OPERATION_TYPES.switch
        }).invoke()
        expect($gameSwitches.value(1)).toEqual(true)
    })

    it('should disable switch switchID=1 via alias "sw[1]=0" switch', () => {
        $gameSwitches.setValue(1, true)
        expect($gameSwitches.value(1)).toEqual(true)
        DataOperationAlias.parseAll("sw[1]=0")[0]?.invoke()
        expect($gameSwitches.value(1)).toEqual(false)
    })

    xit("should nothing to do with switchID=1 via alias sw[1]=2", () =>{})
    xit("should nothing to do with switchID=1 via alias sw[0]=1", () =>{})
    xit("should nothing to do with switchID=1 via alias sw[-1]=0", () =>{})
    xit('should nothing to do switchID=1 via alias "sw[1]=test" switch', () => {})
    xit('should enabled switch switchID=1 via alias "sw[1]=true" switch', () => {})
    xit('should disable switch switchID=1 via alias "sw[1]=false" switch', () => {})
})