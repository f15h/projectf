describe("Operation Alias", () => {
    const tests = [
        {
            input: "ss[A]=1",
            result: [new SelfSwitchOperationAlias({
                value: 1,
                type: OPERATION_TYPES.selfSwitch,
                operation: "=",
                index: "A"
            })]
        },
        {
            input: "sw[1]=0",
            result: [new SwitchOperationAlias({
                value: 0,
                type: OPERATION_TYPES.switch,
                operation: "=",
                index: 1
            })]
        },
        {
            input: "ss[0]=1",
            result: [new SelfSwitchOperationAlias({
                value: 1,
                type: OPERATION_TYPES.selfSwitch,
                operation: "=",
                index: 0
            })]
        },
        {
            input: "self[0]=1",
            result: [new SelfSwitchOperationAlias({
                value: 1,
                type: OPERATION_TYPES.selfSwitch,
                operation: "=",
                index: 0
            })]
        },
        {
            input: "switch[0]=1",
            result: [new SwitchOperationAlias({
                value: 1,
                type: OPERATION_TYPES.switch,
                operation: "=",
                index: 0
            })]
        },
        {
            input: "v[0]=1",
            result: [new VariableOperationAlias({
                value: 1,
                type: OPERATION_TYPES.variable,
                operation: "=",
                index: 0
            })]
        },
        {
            input: "var[0]=1",
            result: [new VariableOperationAlias({
                value: 1,
                type: OPERATION_TYPES.variable,
                operation: "=",
                index: 0
            })]
        },
        {
            input: "variable[0]=1",
            result: [new VariableOperationAlias({
                value: 1,
                type: OPERATION_TYPES.variable,
                operation: "=",
                index: 0
            })]
        },
        {
            input: "v[0]+=1",
            result: [new VariableOperationAlias({
                value: 1,
                type: OPERATION_TYPES.variable,
                operation: "+=",
                index: 0
            })]
        },
        {
            input: "v[0]-=1",
            result: [new VariableOperationAlias({
                value: 1,
                type: OPERATION_TYPES.variable,
                operation: "-=",
                index: 0
            })]
        },
        {
            input: "v[0]=-1",
            result: [new VariableOperationAlias({
                value: -1,
                type: OPERATION_TYPES.variable,
                operation: "=",
                index: 0
            })]
        },
        {
            input: "v[0]=1,v[1]=1",
            result: [
                new VariableOperationAlias({
                    value: 1,
                    type: OPERATION_TYPES.variable,
                    operation: "=",
                    index: 0
                }),
                new VariableOperationAlias({
                    value: 1,
                    type: OPERATION_TYPES.variable,
                    operation: "=",
                    index: 1
                }),
            ]
        },
        {
            input: "v[0]=1 v[1]=1",
            result: [
                new VariableOperationAlias({
                    value: 1,
                    type: OPERATION_TYPES.variable,
                    operation: "=",
                    index: 0
                }),
                new VariableOperationAlias({
                    value: 1,
                    type: OPERATION_TYPES.variable,
                    operation: "=",
                    index: 1
                }),
            ]
        },
        {
            input: "sw[0]=1,sw[1]=1  v[0]=1 v[1]=1, sw[2]=1",
            result: [
                new SwitchOperationAlias({
                    value: 1,
                    type: OPERATION_TYPES.switch,
                    operation: "=",
                    index: 0
                }),
                new SwitchOperationAlias({
                    value: 1,
                    type: OPERATION_TYPES.switch,
                    operation: "=",
                    index: 1
                }),
                new VariableOperationAlias({
                    value: 1,
                    type: OPERATION_TYPES.variable,
                    operation: "=",
                    index: 0
                }),
                new VariableOperationAlias({
                    value: 1,
                    type: OPERATION_TYPES.variable,
                    operation: "=",
                    index: 1
                }),
                new SwitchOperationAlias({
                    value: 1,
                    type: OPERATION_TYPES.switch,
                    operation: "=",
                    index: 2
                }),
            ]
        },
        {
            input: "",
            result: []
        },
        {
            input: undefined,
            result: []
        },
        {
            input: null,
            result: []
        },
    ]

    tests.forEach(parameters => {
        it(`should parse correct '${parameters.input}' string`, () => {
            const result = DataOperationAlias.parseAll(parameters.input)
            expect(result).toEqual(parameters.result)
        })
    })
})