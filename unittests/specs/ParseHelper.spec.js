describe('Parse Helper', () => {
    const tests = [
        {
            input: "-1",
            result: -1
        },
        {
            input: "1",
            result: 1
        },
        {
            input: "0",
            result: 0
        },
        {
            input: "1234566987",
            result: 1234566987
        },

        {
            input: "True",
            result: true
        },
        {
            input: "true",
            result: true
        },
        {
            input: "false",
            result: false
        },
        {
            input: "False",
            result: false
        },
        {
            input: "test",
            result: "test",
        },
        {
            input: "test1",
            result: "test1"
        },
        {
            input: "test 1",
            result: "test 1"
        },
        {
            input: " true false  ",
            result: "true false"
        },
        {
            input: undefined,
            result: ""
        },
        {
            input: null,
            result: ""
        },
    ]

    tests.forEach(parameters => {
        it(`should parse correct '${parameters.input}' string`, () => {
            const result = ParseHelper.stringToType(parameters.input)
            expect(result).toEqual(parameters.result)
        })
    })
})