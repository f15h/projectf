describe('NoteTag', () => {
    const parameters = [
        {
            input: "<canMove>",
            result: [new NoteTag({tag: 'canmove'})
            ]
        },
        {
            input: "<canmove>",
            result: [new NoteTag({tag: 'canmove'})]
        },
        {
            input: "<Canmove>",
            result: [new NoteTag({tag: 'canmove'})]
        },
        {
            input: 'test <canMove onSuccess="ss[A]=1" distance="1" onFail="ss[B]=2,sw[1]=0,var[2]=4"> <test>',
            result: [
                new NoteTag({
                    tag: "canmove",
                    attributes: {
                        'onsuccess': [
                            new SelfSwitchOperationAlias({
                                index: "A",
                                value: 1,
                                type: OPERATION_TYPES.selfSwitch,
                                operation: "="
                            })],
                        'onfail': [
                            new SelfSwitchOperationAlias({
                                index: "B",
                                value: 2,
                                type: OPERATION_TYPES.selfSwitch,
                                operation: "="
                            }),
                            new SwitchOperationAlias({
                                index: 1,
                                value: 0,
                                type: OPERATION_TYPES.switch,
                                operation: "="
                            }),
                            new VariableOperationAlias({
                                index: 2,
                                value: 4,
                                type: OPERATION_TYPES.variable,
                                operation: "="
                            })
                        ],
                        'distance': 1
                    }
                }),
                new NoteTag({
                    tag: 'test'
                })
            ]
        },
        {
            input: 'test',
            result: []
        },
        {
            input: '',
            result: []
        },
        {
            input: undefined,
            result: []
        },
        {
            input: null,
            result: []
        }
    ]
    parameters.forEach((parameter) => {
        it(`should parse correct '${parameter.input}' string`, () => {
            const result = NoteTag.parseAll(parameter.input)
            expect(result).toEqual(parameter.result)
        })

    })

})