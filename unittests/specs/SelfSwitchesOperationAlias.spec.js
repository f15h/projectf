describe('SelfSwitchesOperationAlias', () => {
    it('should enabled self-switch A for eventID=1 via alias "ss[1]=1"', () =>{
        $gameMap._mapId = 1
        const key = [1, 1, "A"]
        $gameSelfSwitches.setValue(key, false)
        expect($gameSelfSwitches.value(key)).toBe(false)
    })

    xit('should disable self-switch A for eventID=1 via alias "ss[1]=0"', () =>{})
    xit('should enabled self-switches for eventID=1 via alias "ss[A]=1"', () =>{})
    xit('should to do nothing self-switches for eventID=1 via alias "ss[0]=1"', () =>{})
    xit('should to do nothing self-switch A for eventID=1 via alias "ss[A]=3"', () =>{})
    xit('should to do nothing self-switches for eventID=1 via alias "ss[E]=3"', () =>{})
    xit('should disable self-switch A for eventID=1 via alias "ss[1]=true"', () =>{})
    xit('should enabled self-switches for eventID=1 via alias "ss[A]=false"', () =>{})
    xit('should to do nothing self-switches for eventID=1 via alias "ss[1]=test"', () =>{})
})