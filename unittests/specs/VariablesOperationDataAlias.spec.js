describe('VariableOperationAlias', () => {
    it('example work with variable', () =>{
        const valueId = 1;
        expect($gameVariables.value(valueId)).toBe(0)
        $gameVariables.setValue(valueId, 1)
        expect($gameVariables.value(valueId)).toBe(1)
    })
})
