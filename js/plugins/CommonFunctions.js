
    (() => {
    function CommonFunction() {
        throw new Error('There is static class')
    }

    /**
     * Common functions for parse gameEntity comments
     *
     * @param {string} comments
     * @return {NoteTag[]}
     */
    CommonFunction.prototype.parseComments = function (comments) {
        const result = NoteTag.parseAll(comments)
        return result
    }
})()