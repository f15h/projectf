/*:
@target MZ
@plugindesc [Version]
@author F15H
@help Set screen resolution

@param width
@type number
@min 0
@default 1200

@param height
@type number
@min 0
@default 1200
 */
(() => {
    const pluginName = "CustomResolution";
    const params = PluginManager.parameters(pluginName);
    const screenWidth = parseInt(params["width"] || 1200)
    const screenHeight = parseInt(params["height"] || 1200)

    Scene_Boot.prototype.resizeScreen = function () {
        Graphics.resize(
           screenWidth,
           screenHeight
        );
        this.adjustBoxSize();
        this.adjustWindow();
    }
})()