/*:
@target MZ
@plugindesc [Version]
@author F15H
@help Fast Forward Improvement

@param factor
@type number
@min 2
@default 2
*/

(() => {
    const _pluginName = "FastForwardImprovement";
    const _params = PluginManager.parameters(_pluginName);
    const _factor = parseInt(_params['factor']) || 2;

    const Alias_Scene_Map_UpdateMainMultiply = Scene_Map.prototype.updateMainMultiply;

    Scene_Map.prototype.updateMainMultiply = function() {
        if (this.isFastForward()) {
            if (_factor === 2) {
                Alias_Scene_Map_UpdateMainMultiply.call(this);
            } else {
                for (let i = 0; i < _factor; i++) {
                    this.updateMain();
                }
            }
        }
        this.updateMain();
    };
})()