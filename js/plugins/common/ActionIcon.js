/*:
@plugindesc
@target MZ
@author GALV updated F15H

@param z
@type number
@min 0
@default 100

@param yOffset
@type number
@default -20

@param opacity
@type number
@default 200
@min 0

@param distance
@type number
@default 300
@min 0

@param maxStrength
@type number
@default 5
@min 1

@command enableGlowForEvents
@desc command to create glow for target events by ID.

@arg targetIds
@desc specify the event id here. (multiple OK)
@type string[]
@default []

@command clearForEvents
@desc command to clear glow for all events.
*/

class ActionIcon {
  static targetEventsIds = []
  static pluginName = 'ActionIcon'
  static params = PluginManager.parameters(ActionIcon.pluginName)

  static needRefresh = false
  static z = Number(ActionIcon.params['z'])
  static yOffset = Number(ActionIcon.params['yOffset'])
  static opacity = Number(ActionIcon.params['opacity'])
  static distance = Number(ActionIcon.params['distance'])
  static maxStrength = Number(ActionIcon.params['maxStrength'])

  static checkAction() {
    let action = null;
    const { _x, _y, _direction } = $gamePlayer

    const x = $gameMap.roundXWithDirection(_x, _direction);
    const y = $gameMap.roundYWithDirection(_y, _direction);

    $gameMap.eventsXy(_x, _y).forEach(event => {
      action = ActionIcon._tryFindActions(event)
    })

    if (!action) {
      $gameMap.eventsXy(x, y).forEach(event => {
        action = ActionIcon._tryFindActions(event)
      })
    }
    action = action || { 'eventId': 0, 'iconId': 0 };
    $gamePlayer.actionIconTarget = action
  }

  static _tryFindActions(event) {
    const pages = event.page()
    if (!pages) return action
    const pagesList = pages.list
    for (let i = 0; i < pagesList.length; i++) {
      const page = pagesList[i]
      if (page.code !== 108) continue
      const iconMatch = page.parameters[0].match(/<actionIcon: (.*)>/i)
      if (!iconMatch) continue
      let rawData = iconMatch[1].split(',')
      const iconId = Number(rawData[0])
      let offset = {
        x: 0,
        y: 0
      }

      if (rawData[1]) {
        const v = rawData[1].split("|")
        if (v[1]) {
          offset.x = Number(v[0]);
          offset.y = Number(v[1]);
        } else {
          offset.y = Number(v[0]);
        }
      }
      return {
        eventId: event._eventId,
        iconId,
        offset
      }
    }
  }
}

(() => {
  // COMMANDS PLUGIN
  //-----------------------------------------------------------------------------
  PluginManager.registerCommand(ActionIcon.pluginName, "clearForEvents", function (args) {
    ActionIcon.targetEventsIds = []
  });

  PluginManager.registerCommand(ActionIcon.pluginName, "enableGlowForEvents", function (args) {
    const targetIds = parseTargetIdArray(args.targetIds, this);
    ActionIcon.targetEventsIds = targetIds
  });

  const parseTargetIdArray = function (idTextArrayText, interpreter) {
    if (idTextArrayText == "" || idTextArrayText == null) return null;

    const idTextArray = JSON.parse(idTextArrayText), targetIdArray = [];
    for (let i = 0; i < idTextArray.length; i++) {
      const num = parseNumberOrDefault(idTextArray[i], true);
      const id = parseTargetId(num, interpreter);
      if (id != null) {
        targetIdArray.push(id);
      }
    }
    return targetIdArray;
  }

  function parseNumberOrDefault(string, isInteger) {
    if (string == null || string === '' || string === 'x') return null;

    isInteger = !!isInteger;
    string = string.replace(' ', '');

    if (string[0] === 'v') {
      const varId = Number(string.slice(1)) || 0;
      const num = $gameVariables.value(varId);
      if (isNaN(num) || num == null) return null;
      if (isInteger) return Math.round(num);
      else return num;
    }
    if (string[0] === 'r') {
      let numPair = string.slice(1).split('~');
      // typo saving
      if (numPair.length < 2) numPair = string.slice(1).split('-');
      let min = parseNumberOrDefault(numPair[0]) || 0,
        max = parseNumberOrDefault(numPair[1]) || 0;
      if (min > max) {
        let temp = min; min = max; max = temp;
      }
      if (isInteger) return Math.floor((Math.random() * (max + 1 - min) + min));
      else return (Math.random() * (max - min) + min);
    }
    const num = Number(string);
    if (isNaN(num) || num == null) return null;
    if (isInteger) return Math.round(num);
    return num;
  };

  const parseTargetId = function (idText, interpreter) {
    if (idText === "" || idText == null) return null;
    let id = Number(idText);
    if (isNaN(id)) return null;

    if (id != 0) return id;
    // id=0: map_this-event / battle_subject
    if (!$gameParty.inBattle()) return (interpreter._eventId || 0);
    let sub = BattleManager._subject, index = 0;
    if (sub) {
      index = $gameTroop.members().indexOf(sub);
      if (index >= 0) {
        index = -(index + 1);
      } else {
        index = $gameParty.members().indexOf(sub);
        if (index < 0) index = 0;
        else index++;
      }
      /*
      if (subject.isActor()) {
        id = subject.actorId();
      } else {
        id = (subject.index());
      }
      */
    }
    return index;
  };
  // GAME SYSTEM
  //-----------------------------------------------------------------------------

  const Game_System_initialize = Game_System.prototype.initialize;
  Game_System.prototype.initialize = function () {
    Game_System_initialize.call(this);
    this.actionIndicatorVisible = true;
  };


  // GAME MAP
  //-----------------------------------------------------------------------------

  const Game_Map_requestRefresh = Game_Map.prototype.requestRefresh;
  Game_Map.prototype.requestRefresh = function (mapId) {
    Game_Map_requestRefresh.call(this, mapId)
    ActionIcon.needRefresh = true
  }

  function distance([x1, y1], [x2, y2]) {
    return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))
  }

  Game_Map.prototype.eventsDistanceXy = function (x, y, d) {
    return this.events().filter(event => {
      const x = event.screenX()
      const y = event.screenY()
      const _x = $gamePlayer.screenX()
      const _y = $gamePlayer.screenY()
      event._distance = distance([x, y], [_x, _y])
      return event._distance < d
    })
  }


  // GAME PLAYER
  //-----------------------------------------------------------------------------

  const Game_CharacterBase_moveStraight = Game_CharacterBase.prototype.moveStraight;
  Game_CharacterBase.prototype.moveStraight = function (d) {
    Game_CharacterBase_moveStraight.call(this, d)
    ActionIcon.needRefresh = true
  };

  const Game_CharacterBase_initialize = Game_CharacterBase.prototype.initialize
  Game_CharacterBase.prototype.initialize = function () {
    Game_CharacterBase_initialize.call(this)
    this.needGlowedEvents = []

  }



  // SPRITESET MAP
  //-----------------------------------------------------------------------------

  const Spriteset_Map_createLowerLayer = Spriteset_Map.prototype.createLowerLayer;
  Spriteset_Map.prototype.createLowerLayer = function () {
    Spriteset_Map_createLowerLayer.call(this)
    this.createActionIconSprite()
  };

  Spriteset_Map.prototype.createActionIconSprite = function () {
    this._actionIconSprite = new Sprite_ActionIcon();
    this._tilemap.addChild(this._actionIconSprite);
  };

  function Sprite_ActionIcon() {
    this.initialize(...arguments);
  }

  Sprite_ActionIcon.prototype = Object.create(Sprite.prototype);
  Sprite_ActionIcon.prototype.constructor = Sprite_ActionIcon;

  Sprite_ActionIcon.prototype.initialize = function () {
    Sprite.prototype.initialize.call(this)
    $gamePlayer.actionIconTarget = $gamePlayer.actionIconTarget || { eventId: 0, iconId: 0, xy: { x: 0, y: 0 } };

    this._index = 0
    // this.glowFilter = new PIXI.filters.GlowFilter()
    // this.glowFilter.outerStrength = 10
    // this.filters = [this.glowFilter]
    this.z = ActionIcon.z
    this.changeBitmap()
    this._tileWidth = $gameMap.tileWidth()
    this._tileHeight = $gameMap.tileHeight()
    this._offsetX = -(ImageManager.iconWidth / 2)
    this._offsetY = -38 + ActionIcon.yOffset
    this.opacity = ActionIcon.opacity
    this.anchor.y = 1;
    this._float = 0.1;
    this.mod = 0.2;
    ActionIcon.needRefresh = true

  }

  Sprite_ActionIcon.prototype.changeBitmap = function () {
    this._index = $gamePlayer.actionIconTarget.eventId <= 0 ? 0 : $gamePlayer.actionIconTarget.iconId

    const width = ImageManager.iconWidth;
    const height = ImageManager.iconHeight;
    const startX = this._index % 16 * width;
    const startY = Math.floor(this._index / 16) * height;

    this.bitmap = new Bitmap(width, height);
    if (this._index <= 0) return

    const bitmap = ImageManager.loadSystem('IconSet');
    this.bitmap.blt(bitmap, startX, startY, width, height, 0, 0);

    ActionIcon.needRefresh = false;
  }

  Sprite_ActionIcon.prototype.initAnimVariables = function () {
    this.scale.y = 0.1
    this.opacity = 0
    this.mod = 0.2
    this._float = 0.1

  }
  Sprite_ActionIcon.prototype.updateOpacity = function () {
    if ($gameMap.isEventRunning()) {
      this.opacity -= 40
    } else {
      this.opacity = $gameSystem.actionIndicatorVisible ? ActionIcon.opacity : 0
    }
  }

  Sprite_ActionIcon.prototype.update = function () {
    Sprite.prototype.update.call(this)

    if (ActionIcon.needRefresh) {
      ActionIcon.checkAction()
    }

    if ($gamePlayer.actionIconTarget.eventId != this._eventId) {
      this.initAnimVariables()
      this._eventId = $gamePlayer.actionIconTarget.eventId
    }

    if (this._index !== $gamePlayer.actionIconTarget.iconId) this.changeBitmap()
    if (this._index <= 0) return

    const commentX = $gamePlayer.actionIconTarget.offset.x
    const commentY = $gamePlayer.actionIconTarget.offset.y
    this.x = $gameMap.event($gamePlayer.actionIconTarget.eventId).screenX() + this._offsetX + commentX
    this.y = $gameMap.event($gamePlayer.actionIconTarget.eventId).screenY() + this._offsetY + this._float + commentY
    this.scale.y = Math.min(this.scale.y + 0.1, 1)
    this.updateOpacity()
    this._float += this.mod
    if (this._float < -0.1) {
      this.mod = Math.min(this.mod + 0.01, 0.2)
    } else if (this._float >= 0.1) {
      this.mod = Math.max(this.mod + -0.01, -0.2)
    }
  }

  function normalize(val, max, min) {
    return (val - min) / (max - min)
  }

  Game_CharacterBase.prototype.type = function () {
    return "Game_CharacterBase"
  }

  Game_Event.prototype.type = function () {
    return "Game_Event"
  }

  Game_Event.prototype.isPlayerInRange = function (range) {
    const { _x, _y } = $gamePlayer
    const { x, y } = this
    return distance([x, y], [_x, _y]) <= range
  }

  Game_Event.prototype.actionIcon = function () {
    return ActionIcon._tryFindActions(this)
  }

  Game_Event.prototype.calculateGlowStrength = function () {
    const eScreenX = this.screenX()
    const eScreenY = this.screenY()
    const pScreenX = $gamePlayer.screenX()
    const pScreenY = $gamePlayer.screenY()
    const pixelDistance = distance([eScreenX, eScreenY], [pScreenX, pScreenY])
    return (1 - normalize(pixelDistance, ActionIcon.distance, 1)) * ActionIcon.maxStrength
  }

  Sprite_Character.prototype.updateGlowStrength = function () {
    if (!this.glowFilter) {
      if (this._character._eventId == 2) debugger
      this.glowFilter = new PIXI.filters.GlowFilter()
      this.filters = [this.glowFilter]

    }
    if (!this.glowFilter.enabled) {
      this.glowFilter.enabled = true
    }
    this.glowFilter.outerStrength = this._character.calculateGlowStrength()
  }

  Sprite_Character.prototype.disableGlowFilter = function () {
    if (this.glowFilter) {
      this.glowFilter.enabled = false
    }
  }

  Sprite_Character.prototype.updateGlow = function () {
    const event = this._character
    if (event.type() !== "Game_Event") return

    if (!event.isPlayerInRange(20)) return

    const isHasAction = event.actionIcon()
    const isNeedGlow = ActionIcon.targetEventsIds.indexOf(event._eventId) > -1
    if (isHasAction && isNeedGlow) {
      this.updateGlowStrength()
    } else {
      this.disableGlowFilter()
    }
  }

  const Sprite_Character_update = Sprite_Character.prototype.update
  Sprite_Character.prototype.update = function () {
    Sprite_Character_update.call(this)
    this.updateGlow()
  }
})()