/*:
@target MZ
@plugindest 'Show Text' windows float message

@author F15H

@param fontSize
@type number
@min 14
@default 14

@param WindowStyle
@type string
@default none

@param yOffset
@type number
@default 42

@param xOffset
@type number
@default -100

@param lineOffset
@type number
@default 2
@min 1
 */


class WindowMessageHelper {
  /**
   * Class Construtor
   * @param {Object} options
   * @param {string} options.pluginName Name of plugin for load paramaters
   */
  constructor(options) {
    this.__pluginName = options.pluginName

    this.params = PluginManager.parameters(this.__pluginName)

    this.__target = null
    this.__eventId = null

    this.refreshOnClose = false;

    this.screenXOffset = 0
    this.screenYOffset = 0

    this.__yOffset = Number(this.params['yOffset'])
    this.__xOffset = Number(this.params['xOffset'])

    this.__fontSize = Number(this.params['fontSize'])
    this.__lineOffset = Number(this.params['lineOffset'])
    this.lineHeight = this.fontSize * this.__lineOffset

    this.testActive = false
  }

  get yOffset() {
    return this.__yOffset
  }

  get xOffset() {
    return this.__xOffset
  }

  get fontSize() {
    return this.__fontSize
  }

  get padding() {
    return 0
  }


  set event(e) {
    this.__eventId = e
  }

  get event() {
    return this.__eventId
  }

  get pluginName() {
    return this.__pluginName
  }

  /**
   *
   * @param {Object|null} options
   * @param {string=} options.id Target id
   * @param {string=} options.x screenX
   * @param {string=} options.y screenY
   */
  set target(options) {
    let { id, x, y } = options
    if (y) {
      this.__target = {
        _x: Number(x),
        _y: Number(y),
        screenX: function () {
          return this._x
        },
        screenY: function () {
          return this._y
        },
      }
      return
    }
    id = Number(id)
    switch (id) {
      case 0:
        this.__target = $gameMap.event(this.__eventId)
        break
      case -1:
        this.__target = $gamePlayer
        break
      default:
        this.__target = $gameMap.event(id)
        break
    }
  }

  get target() {
    return this.__target
  }

  _parseMessageText(text) {
    let buf = text.replace(/\\/g, "\x1b");
    buf = buf.replace(/\x1b\x1b/g, "\\");
    buf = buf.replace(/\x1bV\[(\d+)\]/gi, (_, p1) =>
      $gameVariables.value(parseInt(p1))
    );
    let rawData = buf.match(/\x1bpop\[[^\]]*\]/gi);
    if (rawData) {
      const data = rawData[0].match(/\[(.*)\]/)[1].split(",");
      const d = data[0].split('|')
      if (!d[1]) {
        this.target = { id: d[0] }
      } else {
        this.target = { x: d[0], y: d[1] }
      }
    }
  }

  resetTargets() {
    this.__target = null
    this.__eventId = null
  }

  static refreshMessageWindows = function () {
    if (SceneManager._scene._messageWindow) SceneManager._scene._messageWindow.refreshGraphics();
    if (SceneManager._scene._nameWindow) SceneManager._scene._nameBoxWindow.refreshGraphics();
  }

  /**
   * 
   * @param {number} v1 
   * @param {number} v2 
   */
  static zoomOffset(v1, v2) {
    return (v1 - v2) - (v1 - v2) * $gameScreen.zoomScale();
  }


}

(() => {

  const pluginName = 'ImpruvedMessageStyles'
  const popupMsg = new WindowMessageHelper({ pluginName })
  const old = {}


  old.Window_Base_lineHeight = Window_Base.prototype.lineHeight
  Window_Base.prototype.lineHeight = function () {
    if (popupMsg.target) {
      return popupMsg.lineHeight
    }

    return old.Window_Base_lineHeight.call(this)
  }

  old.command101 = Game_Interpreter.prototype.command101
  Game_Interpreter.prototype.command101 = function (params) {
    const parameters = this._list[this._index + 1].parameters
    const isPopup = parameters[0].match(/\\pop/i)
    if (isPopup) {
      popupMsg.event = this._eventId
      popupMsg._parseMessageText(parameters[0])

      if (popupMsg.target == null) {
        while (this.nextEventCode() === 401) {  // Text data
          this._index++;
        }
        return false;
      }

    } else {
      popupMsg.resetTargets()
    }
    return old.command101.call(this, params)
  }

  old.Window_Message_initialize = Window_Message.prototype.initialize
  Window_Message.prototype.initialize = function (rect) {
    old.Window_Message_initialize.call(this, rect)
    this.loadWindowskin()
    popupMsg.screenXOffset = (Graphics.width - Graphics.boxWidth) / 2;
    popupMsg.screenYOffset = (Graphics.height - Graphics.boxHeight) / 2;
  }


  old.Window_Message_loadWindowskin = Window_Message.prototype.loadWindowskin
  Window_Message.prototype.loadWindowskin = function () {
    if (popupMsg.target) {
      this._windowskin = ImageManager.loadSystem("Window");
      return;
    }

    old.Window_Message_loadWindowskin.call(this)
  }
  Window_Message.prototype.refreshGraphics = function () {
    // load skin
    this.loadWindowskin()
    this.contents.clear()
    this.updateTone()
    this.changeWindowDimensions()
    this.createContents()
    this.openness = 0
  }
  old.Window_Message_startMessage = Window_Message.prototype.startMessage
  Window_Message.prototype.startMessage = function () {
    old.Window_Message_startMessage.call(this)
    if (popupMsg.target) {
      this.windowskin = ImageManager.loadSystem("Invisible")
    } else {
      this.windowskin = ImageManager.loadSystem("Window")
    }
  }



  old.Window_Message_update_close = Window_Message.prototype.updateClose
  Window_Message.prototype.updateClose = function () {
    if (popupMsg.refreshOnClose && this.isClosed()) {
      popupMsg.refreshOnClose = false
      WindowMessageHelper.refreshMessageWindows()
    }
    old.Window_Message_update_close.call(this)
  }

  old.window_message_message_start = Window_Message.prototype.messageStart
  Window_Message.prototype.messageStart = function () {
    old.window_message_message_start.call(this)
    this.changeWindowDimensions();
    this.refreshDimmerBitmap();
    this.setPopSettings();
  }

  old.window_message_update_placement = Window_Message.prototype.updatePlacement
  Window_Message.prototype.updatePlacement = function () {
    if (popupMsg.target) {
      this.pTarget = popupMsg.target
      this.updateFloatPlacement()
    } else {
      this.pTarget = null
      this.changeWindowDimensions()
      old.window_message_update_placement.call(this)
    }
  }

  old.window_message_update = Window_Message.prototype.update
  Window_Message.prototype.update = function () {
    old.window_message_update.call(this)
    this.updateFloatPlacement()
  }

  old.window_message_terminateMessage = Window_Message.prototype.terminateMessage
  Window_Message.prototype.terminateMessage = function () {
    old.window_message_terminateMessage.call(this)
    this.pTarget = null
    popupMsg.resetTargets()
  }

  Window_Message.prototype.posX = function () {
    const x = this.pTarget.screenX() - popupMsg.screenYOffset
    const zoomX = WindowMessageHelper.zoomOffset(Graphics.boxWidth / 2, x)
    return x - this.width / 2 + zoomX - popupMsg.xOffset
  }

  Window_Message.prototype.outputHeight = function () {
    if (this._textState) {
      this._outputHeight = this._textState.outputHeight
    }
    return this._outputHeight
  }
  Window_Message.prototype.posY = function () {
    let y = this.pTarget.screenY() - popupMsg.screenYOffset

    const zoomY = WindowMessageHelper.zoomOffset(Graphics.boxHeight / 2, y)
    const result = y - this.height / 2 + zoomY - this.outputHeight()
    return result
  }

  Window_Message.prototype.updateFloatPlacement = function () {
    if (this.pTarget == null) return;
    this.contents.fontSize = popupMsg.fontSize
    this.x = this.posX()
    this.y = this.posY()

    this.updateOtherMsgWindows()
  }

  Window_Message.prototype.updateOtherMsgWindows = function () {
    this.updateFloatGoldWindow();
    this.updateFloatChoiceWindow();
  };

  Window_Message.prototype.updateFloatGoldWindow = function () {
    if (!this._goldWindow) return;
    this._goldWindow.y = this._nameBoxWindow.y < this._goldWindow.height ? Graphics.boxHeight - this._goldWindow.height : 0;
  };


  Window_Message.prototype.updateFloatChoiceWindow = function () {
    const positionType = $gameMessage.choicePositionType()
    const setY = this.y + this.height
    if (setY + this._choiceListWindow.height > Graphics.height) {
      // If choice window goes off bottom of screen.
      this._choiceListWindow.y = this.y - this._choiceListWindow.height
    } else {
      this._choiceListWindow.y = setY
    }
    switch (positionType) {
      case 0: // left
        this._choiceListWindow.x = this.x
        break
      case 1: // middle
        this._choiceListWindow.x = this.x + (this.width / 2) - this._choiceListWindow.width / 2;
        break
      case 2: // right
        this._choiceListWindow.x = this.x + this.width - this._choiceListWindow.width;
        break
    };
  };

  old.Window_Message_newLineX = Window_Message.prototype.newLineX
  Window_Message.prototype.newLineX = function (textState) {
    const padding = 0
    return old.Window_Message_newLineX.call(this, textState) + padding
  }

  old.window_message_newPage = Window_Message.prototype.newPage
  Window_Message.prototype.newPage = function (textState) {
    old.window_message_newPage.call(this, textState)
    textState.y += popupMsg.padding

  }

  Window_Message.prototype.changeWindowDimensions = function () {
    if (this.pTarget == null) {
      // normal msg window

      this.yOffset = 0
      this.width = this.windowWidth()
      this.height = this.fittingHeight(4) + popupMsg.padding
      this.x = (Graphics.boxWidth - this.width) / 2
      return
    }
    popupMsg.testActive = true
    let w = 0
    let h = 0
    this.contents.fontSize = 12

    const xO = 8

    for (let i = 0; i < $gameMessage._texts.length; i++) {
      let lineWidth = this.textWidthEx($gameMessage._texts[i])
      if (w < lineWidth) w = lineWidth
    };
    w = w + $gameSystem.windowPadding() * 2 + xO
    this.width = Math.min(Graphics.boxWidth, w)
    let textState = { index: 0 }
    const lines = $gameMessage._texts

    const extraFontHeight = 0
    for (let i = 0; i < lines.length; i++) {
      let up = (lines.match(/\\{/g) || []).length
      let down = (lines.match(/\\}/g) || []).length
      extraFontHeight += ((up - down))
    }
    extraFontHeight *= 2

    testState.text = this.convertEscapeCharacters($gameMessage.allText)

    const allLineHeight = this.calcTextHeight(textState, true)
    const height = allLineHeight + $gameSystem.windowPadding() + extraFontHeight;
    const minHeight = this.fittingHeight($gameMessage._texts.length)
    this.height = Math.max(height, minHeight)
    this.yOffset = this.height
    popupMsg.testActive = false
  }

  old.window_message_process_escapedCharater = Window_Message.prototype.processEscapeCharacter
  Window_Message.prototype.processEscapeCharacter = function (textState) {
    old.window_message_process_escapedCharater.call(this, textState)
  }

  Window_Message.prototype.windowWidth = function () {
    return Graphics.boxWidth
  }

  Window_Message.prototype.windowWidthEx = function (text) {
    return this.textSizeEx(text).width
  }

  old.convertEscapeCharacters = Window_Message.prototype.convertEscapeCharacters
  Window_Message.prototype.convertEscapeCharacters = function (text) {
    text = text.replace(/\\pop\[.*\]/gi, "")
    text = old.convertEscapeCharacters.call(this, text)

    return text
  }

  old.Window_Message__refreshPauseSign = Window_Message.prototype._refreshPauseSign;
  Window_Message.prototype._refreshPauseSign = function () {
    old.Window_Message__refreshPauseSign.call(this);
    // indicator graphic to let user know they need to press a button
    let x = 0;
    let y = 0;
    const oX = 0;
    const oY = 0;

    this._pauseSignSprite.anchor.y = 0.5;
    this._pauseSignSprite.anchor.x = 0.5;

    const pos = 0;

    switch (pos) {
      case 1:
        x = oX;
        y = this._height + oY;
        break;
      case 2:
        x = this._width / 2 + oX;
        y = this._height + oY;
        break;
      case 3:
        x = this.width + oX;
        y = this._height + oY;
        break;
      case 4:
        x = oX;
        y = this.height / 2 + oY;
        break;
      case 6:
        x = this.width + oX;
        y = this.height / 2 + oY;
        break;
      case 7:
        x = oX;
        y = oY;
        break;
      case 8:
        x = this._width / 2 + oX;
        y = oY;
        break;
      case 9:
        x = this.width + oX;
        y = oY;
        break;
    }
    this._pauseSignSprite.move(x, y);
  };
})()