//=============================================================================
// RPG Maker MZ - Custom Data Tags
// ----------------------------------------------------------------------------
// (C)2022
// This software is released under the MIT License.
// http://opensource.org/licenses/mit-license.php
// ----------------------------------------------------------------------------
// Version
// 1.0.0
// ----------------------------------------------------------------------------
// [GitLab]:https://gitlab.com/f15h/projectf
//=============================================================================

/*:
 * @target MZ
 * @plugindesc Custom Data Tags
 * @author F15H
 *
 * @help tags.js
 *
 * This plugin parses various tags in notes of objects.
 * A tag has the form
 * <tag attribute1 attribute2 ... attribute n>
 *
 */

class NoteTag {

    /**
     * Class constructor
     *
     * @param {Object}      options
     * @param {string}      options.tag             - tag name
     * @param {Object=}     options.attributes      - attributes of tag
     *
     */
    constructor(options) {
        this._attributes = options.attributes || {}
        this._name = options.tag
    }

    /**
     * Getting attribute value by name
     *
     * @param {string} attrName of attribute
     *
     * @return {string|null} value of attribute or {@link null} if attribute don't exist
     */
    getAttribute(attrName) {
        return this._attributes[attrName.toLowerCase()] || null
    }

    /**
     * Getting name of tag
     *
     * @return {string} name of tag
     */
    get name() {
        return this._name;
    }

    /**
     * Static method for parse values from event.notes()
     *
     * @param notes {string} note of event
     *
     * @return {NoteTag[]} list of {@link NoteTag}
     */
    static parseAll(notes) {
        if (!notes) {
            return []
        }
        function parseContent(content) {
            const result = {
                tag: '',
                attributes: {}
            }
            const tagRegExp = /<(\S+).*>/
            const match = tagRegExp.exec(content)
            if (match !== null) {
                const [full, ...captures] = match
                result.tag = captures[0].toLowerCase()
            } else {
                return;
            }

            const attributeRegExp = /\s*([a-zA-Z][\w:\-]*)(?:\s*=(\s*"(?:\\"|[^"])*"|\s*'(?:\\'|[^'])*'|[^\s>]+))?/
            let run = true
            let buffer = content;
            while (run) {
                const match = attributeRegExp.exec(buffer)
                if (match !== null) {
                    const [full, ...captures] = match
                    const attribute = captures[0].toLowerCase()
                    if (attribute !== result.tag) {
                        const value = ParseHelper.stringToType(captures[1]);
                        const parsedDataAliases = DataOperationAlias.parseAll(value);
                        result.attributes[attribute] = parsedDataAliases.length > 0 ? parsedDataAliases : value
                    }
                    buffer = buffer.replace(full, '') // TODO: replace not optimal solution
                } else {
                    run = false;
                }
            }
            return result;
        }

        function parseNote(content) {
            const result = []
            const regExp = /<(?:"[^"]*"['"]*|'[^']*'['"]*|[^'">])+>/g
            const matches = content.matchAll(regExp);
            for (const match of matches) {
                const { tag, attributes } = parseContent(match.toString())
                result.push(new NoteTag({ attributes, tag }))
            }
            return result;
        }
        return parseNote(notes)
    }
}

const OPERATION_TYPES = Object.freeze(
    {
        variable: 'variable',
        switch: 'switch',
        selfSwitch: 'selfSwitch'
    }
)

/**
 * Class contains different helper functions for correct parsing values to text to different types
 */
class ParseHelper {
    static __regex = {
        isBoolean: /^(true|false)$/i,
        isString: /^"(.*?)"$/,
        isNumber: /^[-'"]?\d+[-'"]?$/,
        isFloat: /^-?\d+\.?\d*$/,
        isPoint: /^\((-?\d+\.?\d*),\s*(-?\d+\.?\d*)\)$/,
        isArray: /^\[(.*?)\]$/,
        isObj: /^\{(.*?)\}$/
    }

    constructor() {
        throw new Error("There is static class")
    }

    /**
     * Method convert try save convert string to different types
     *
     * @param {string} input input string
     *
     * @return {boolean|string|number} converted data
     */
    static stringToType(input) {

        if (!input) {
            return ""
        }
        let buffer = input.trim()

        const rx = ParseHelper.__regex;

        if (rx.isBoolean.test(buffer)) {
            return buffer.toLowerCase() === 'true'
        }
        if (rx.isNumber.test(buffer)) {
            const result = +`"${buffer}"`.match(/\d+/)
            return result
        }
        return buffer
    }
}

class DataOperationAlias {
    static regex = /(?<type>v|var|variable|ss|self|sw|switch)(:?\[\s*(?<index>"[^"]*"|[\dA-Da-d]+)(\s*,\s*("[^"]*"|\d+))*\s*\])(?<operation>\=|\-\=|\+\=)(?<value>[-]?\d+)/

    /**
     * Class constructor
     *
     * @param options {Object} rawData
     *
     * @param {string} options.type                               - type of alias
     * @param {number} options.value                              - value for operations
     * @param {number|string} options.index                              - index for variable or switch
     * @param {string} options.operation                          - operations
     */
    constructor(options) {
        this.__type = options.type;
        this.__index = options.index;
        this.__value = options.value;
        this.__operation = options.operation
    }

    get type() {
        return this.__type;
    }

    /**
     * Process operation
     */
    invoke() {

    }

    /**
     * Method for parse text and getting {@link DataOperationAlias} if exists
     *
     * @param {string} input raw string for parse
     *
     * @return {DataOperationAlias[]} List of {DataAlias} objects
     */
    static parseAll(input) {
        const result = []

        let run = true

        /**
         * Method for getting {@link DataOperationAlias} from raw string
         *
         * @param raw
         * @return {*|string}
         */

        function processType(raw) {
            const types = {
                'var': OPERATION_TYPES.variable,
                'v': OPERATION_TYPES.variable,
                'variable': OPERATION_TYPES.variable,

                'sw': OPERATION_TYPES.switch,
                'switch': OPERATION_TYPES.switch,

                'ss': OPERATION_TYPES.selfSwitch,
                'self': OPERATION_TYPES.selfSwitch,

                'default': ''
            }
            return types[raw] || types['default']
        }

        /**
         * Fabric method for getting a class instance by type
         *
         * @param {string} type raw string contains type of DataOperationAlias
         * @return {Function} Class instance
         */
        function fabricMethod(type) {
            let obj
            switch (type) {
                case OPERATION_TYPES.variable:
                    obj = VariableOperationAlias
                    break
                case OPERATION_TYPES.switch:
                    obj = SwitchOperationAlias
                    break
                case OPERATION_TYPES.selfSwitch:
                    obj = SelfSwitchOperationAlias
                    break
                default:
                    console.error(`invalid type for '${type}' for DataOperationAlias`)
            }
            return obj
        }

        while (run) {
            const match = DataOperationAlias.regex.exec(input)
            if (match) {
                const [
                    full,
                    ..._
                ] = match

                let {
                    groups: {
                        index,
                        value,
                        operation,
                        type
                    }
                } = match
                index = ParseHelper.stringToType(index)
                value = ParseHelper.stringToType(value)
                operation = ParseHelper.stringToType(operation)
                type = processType(type)
                const cls = fabricMethod(type)
                result.push(new cls({
                    index,
                    value,
                    operation,
                    type
                }))
                input = input.replace(full, '')
            } else {
                run = false
            }
        }

        return result
    }
}

class SelfSwitchOperationAlias extends DataOperationAlias {
    invoke() {
        switch (this.__operation) {
            case '=':
                throw new Error('SelfSwitchOperationAlias::invoke not implemented')
            default:
                console.error(`there is wrong operation ${this.__operation}`)
        }
    }
}

class SwitchOperationAlias extends DataOperationAlias {
    invoke() {
        switch (this.__operation) {
            case '=':
                $gameSwitches.setValue(this.__index, this.__value === 1 || this.__value === true)
                break
            default:
                console.error(`there is wrong operation ${this.__operation}`)
        }
    }
}
class VariableOperationAlias extends DataOperationAlias {
    invoke() {
        let value;
        switch (this.__operation) {
            case '=':
                $gameVariables.setValue(this.__index, this.__value)
                break
            case '+=':
                value = $gameVariables.value(this.__index) + this.__value
                $gameVariables.setValue(this.__index, value)
                break
            case '-=':
                value = $gameVariables.value(this.__index) - this.__value
                $gameVariables.setValue(this.__index, value)
                break
            default:
                console.error(`there is wrong operation ${this.__operation} for type "${this.type}`)
        }

    }
}
