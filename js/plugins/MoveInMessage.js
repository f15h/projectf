/*:
@target MZ
@plugindesc [Version]
@author F15H
@help Move In Messages

@command enable

@command disable
*/
(() => {
    const _pluginName = 'MoveInMessage';

    let canMove = true;

    const Alias_Game_Player_canMove = Game_Player.prototype.canMove;


    Game_Player.prototype.canMove = function () {

        if (false === this.isCanMove()) {
            return Alias_Game_Player_canMove.call(this);
        }

        if (this.isMoveRouteForcing() || this.areFollowersGathering()) {
            return false;
        }
        if (this._vehicleGettingOn || this._vehicleGettingOff) {
            return false;
        }
        if (this.isInVehicle() && !this.vehicle().canMove()) {
            return false;
        }
        return true;
    }

    Game_Player.prototype.isCanMove = function () {

        if (!$gameMap.isEventRunning()) return false

        const eventId = $gameMap._interpreter.eventId()
        if (eventId == 0) return false

        const activeEvent = $gameMap.event(eventId)
        if (!activeEvent) return false

        const notes = activeEvent.notes()
        let canMoveTag = notes.filter(note => note._name === 'canmove')
        if (canMoveTag.length <= 0) return false
        return true
    }

    Game_Event.prototype.notes = function () {
        return this._noteTags
    }

    const old_Game_Event_initialize = Game_Event.prototype.initialize
    Game_Event.prototype.initialize = function (mapId, eventId) {
        old_Game_Event_initialize.call(this, mapId, eventId)
        const note = $dataMap.events[eventId]?.note
        this._noteTags = NoteTag.parseAll(note)
    }

    const Game_Interpreter_nextEventCode = Game_Interpreter.prototype.nextEventCode
    Game_Interpreter.prototype.nextEventCode = function () {
        return Game_Interpreter_nextEventCode.call(this)
    }

    PluginManager.registerCommand(_pluginName, "enable", args => {
        canMove = true
    });


    PluginManager.registerCommand(_pluginName, "disable", args => {
        canMove = false
    });
})()